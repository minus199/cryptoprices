const logger = require("winston")
const GoogleSpreadsheet = require('google-spreadsheet')
const { sheetID } = require("../../../resources/gspreadConfig")

const AsyncDataModelFactory = sheet => new Proxy(sheet, sheetProxyHandler)

const GSheetsClient = function () {
  this.docClient = new GoogleSpreadsheet(sheetID)
  this.is_init = false
  this.googleFile = null
  this.dataModels = new Map()
}

GSheetsClient.prototype.init = async function init() {
  if (!this.is_init) {
    this.googleFile = await new Promise((rez, rej) => {
      const creds = require('../../../resources/client_secret.json')
      logger.debug('Starting GSheets auth')

      this.docClient.useServiceAccountAuth(creds, () => this.docClient.getInfo((err, info) => err ? rej(err) : rez(info)))
    })
  }

  this.is_init = true
  return this
}

GSheetsClient.prototype.getAll = async function getAll() {
  if (this.dataModels.size == 0) {
    this.googleFile.worksheets.forEach(worksheet => {
      const title = worksheet.title
      const slug = `${title[0].toLowerCase()}${title.slice(1, title.length)}` // lowercase first letter...
      return this.dataModels.set(slug, worksheet)
    })
  }

  return this.dataModels
}

/**
 * @returns {{portfolio}, {rates}, {profitHistogram},{coinsMeta}}
 */
GSheetsClient.prototype.run = async function () {
  return this.init().then(gClient => gClient.getAll())
}

const client = new GSheetsClient()

module.exports = async () => client.run()