const logger = require("winston")
const { DataModel } = require("./DataModel")
const dataProvider = require("./gsheets")

const DBProxyHandler = {
    /**
     * @param {cloudDataModel DataModel}
     */
    get: function (db, modelName) {
        return db.dataModels.get(modelName) || DB.EMPTY_TABLE
    },

    set: function (db, modelName, dataModel) {
        if (!dataModel instanceof DataModel)
            throw new Error(`Instance mismatch - Not ${dataModel.__proto__.constructor.name}`)

        const currentValue = db.dataModels.get(modelName)
        if (!dataModel) throw new Error("Unable to set empty data model")

        if (currentValue === dataModel) {
            logger.warning(`updating dataModel of ${currentValue} into ${dataModel}`)
        }

        db.dataModels.set(modelName, dataModel)
        return db
    },

    deleteProperty: function (db, modelName) {
        db.dataModels.delete(modelName)
        return this
    },

    enumerate: function (db, modelName) {
        return db.dataModels.keys()
    },

    ownKeys: function (db, modelName) {
        return Array.from(db.dataModels.keys())
    },

    has: function (db, modelName) {
        return db.dataModels.has(dataModel)
    },

    defineProperty: function (db, modelName, oDesc) {
        throw new Error("not supported")
    },

    getOwnPropertyDescriptor: function (db, modelName) {
        return !db.dataModels.has(modelName) ? undefined : {
            value: db.dataModels.get(modelName),
            writable: true,
            enumerable: true,
            configurable: true
        }
    }
}

const DB = function (dataModels) {
    dataModels.forEach((dataModel, dmName) => this.dataModels.set(dmName, new DataModel(dataModel)))
}

DB.instance = null
DB.prototype.dataModels = new Map()

DB.prototype.buildRatesTable = async function buildRatesTable(coinsMeta) {
    const _rateDM = await this.ratesDataModel.clear().
        setColumnNames(coinsMeta.fiat.concat(Object.keys(coinsMeta.crypto)))

    const updateRes = Object.keys(rates).map(async rateEnum => {
        const rowData = rates[rateEnum]
        rowData.enum = rateEnum
        return await ratesDataModel.addRow(rowData)
    })
}

const dbInstanceProvider = module.exports = async () => {
    if (!DB.instance) {
        const dataModels = await dataProvider()
        const db = new DB(dataModels)
        DB.instance = new Proxy(db, DBProxyHandler)
    }

    return DB.instance
}