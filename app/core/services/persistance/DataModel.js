const logger = require("winston")
const DataModel = function DataModel(cloudDataModel) {
    this.tableName = ''
    this.cloudDataModel = cloudDataModel
}

DataModel.prototype.create = async function create(row) {
    return await new Promise((rez, rej) => this.cloudDataModel.addRow(row, (err, res) => err
        ? rej(err)
        : rez(res)))
}

DataModel.prototype.setColumnNames = async function setColumnNames(dataModel, ...colNames) {
    await new Promise((rez, rej) => this.cloudDataModel.setHeaderRow(...colNames, (err, res) => err
        ? rej(err)
        : rez(res)))

    return this
}

DataModel.prototype.clear = async function clear() {
    try {
        await this.cloudDataModel.clear((err, res) => new Promise((rez, rej) => err
            ? rej(err)
            : rez(res)))

    } catch (err) {
        console.error("Failed to clear data table", err)
        // process.exit(420)
    }

    return this
}

DataModel.prototype.getRows = async function getRows() {
    return new Promise((rez, rej) => {
        this.cloudDataModel.getRows({}, (err, rows) => err ? rej(err) : rez(rows))
    })
}

module.exports = {
    EMPTY: new DataModel(null), DataModel
}