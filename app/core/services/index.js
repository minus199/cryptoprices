// Services index

module.exports = {
    persistanceService: require("./persistance"),
    coinService: require("./coinService"),
    coinFactory: require("./coinFactory")
}