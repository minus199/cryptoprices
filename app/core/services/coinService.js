const logger = require("winston")
const axios = require("axios")
const coinFactory = require("./coinFactory")

const CoinService = function () {
    this.persistanceService = require("./persistance") // avoid cyclic issues
}

CoinService.prototype.computeCorrelations = async function (coinsMeta, ratesDataModelRowsBySlug) {
    return coinsMeta.getRows().then(rows => rows.reduce((acc, coinMetaRow, i, rowCols) => {
        if (coinMetaRow.isfiat === "1") {
            acc.fiat.push(coinMetaRow.slug)
            return acc
        }

        acc.crypto[coinMetaRow.slug] = rowCols.
            filter(row => row.slug !== coinMetaRow.slug).
            map(row => row.slug)

        // Fetch current rates for all coins
        const fetchUpdateRatesTask = this.fetchRates(coinMetaRow.slug, acc.crypto[coinMetaRow.slug]).
            then(({ slug, rates }) => {
                // Write rates for each coin into cloud
                const row = ratesDataModelRowsBySlug[slug]
                Object.keys(rates).forEach(bound => {
                    logger.debug(`Rates for ${coinMetaRow.slug} in ${bound} were updated from ${row[bound.toLowerCase()]} to ${rates[bound]}`)
                    row[bound.toLowerCase()] = rates[bound]
                })
                row.save()

                return { slug, rates }
            })

        acc.pendingRates.push(fetchUpdateRatesTask)
        return acc

    }, { fiat: [], crypto: {}, pendingRates: [] }))
}

CoinService.prototype.fetchRates = async function fetchRates(slug, bound) {
    logger.debug(`Fetching latest rates for ${slug}`)
    // { json: true },
    return await axios.get(`https://min-api.cryptocompare.com/data/price?fsym=${slug}&tsyms=${bound.join(",")}`)
        .then(response => {
            logger.debug(`* Got rates for ${slug} in ${Object.keys(response.data).join(', ')}`, response.data)
            return { slug, rates: response.data }
        })

        .catch(err => {
            logger.error(`Fetching for ${slug} failed`, err)
            return { err, slug }
        })
}

CoinService.prototype.normalizeRatesDataModel = async function normalizeRates(ratesDataModel) {
    return ratesDataModel.getRows().then(rows => rows.reduce((acc, ratesRow, i, colRows) => {
        logger.debug(`normalizeRatesDataModel ${i + 1}/${colRows.length} -- ${ratesRow.enum}`)
        acc[ratesRow.enum] = ratesRow
        return acc
    }, {}))
}

CoinService.prototype.fetchPortfolioRates = async function fetchAllRates(correlations) {
    const ratesTasks = Object.keys(correlations).map(slug => getRatesRequestTask(slug, correlations[slug]))

    return Promise.all(ratesTasks).then(currentRates => currentRates.reduce((acc, coinRates) => {
        acc[coinRates.slug] = coinRates.rates
        return acc
    }, {}))
}

CoinService.prototype.coinsBindingsFactory = async function coinsBindingsFactory(coinsMetaDataModel, ratesDataModel) {
    const ratesDataModelsBySlug = await coinService.normalizeRatesDataModel(ratesDataModel)
    const { fiat, crypto, pendingRates } = await coinService.computeCorrelations(coinsMetaDataModel, ratesDataModelsBySlug)

    return {
        coins: { fiat, crypto },
        ratesDataModelsBySlug,
        resolvedRates: await Promise.all(pendingRates)
    }
}

CoinService.prototype.normalizePortfolio = async function normalizePortfolio(portfolioDataModel) {
    const portfolioDataComputers = {
        computeInitialInvestment(rawPortfolioData) {
            const initialInvestment = {
                rate: rawPortfolioData.buyrate.asNumeric(),
                amount: rawPortfolioData.numunits.asNumeric(),
                value: rawPortfolioData.buytotal.asNumeric()
            }
            this.initialInvesment.push(initialInvestment)
        },

        computeProfit(rawPortfolioData) {
            const profit = {
                percentageDelta: rawPortfolioData.totalprofit.asNumeric() / rawPortfolioData.buytotal.asNumeric(),
                absDelta: rawPortfolioData.totalprofit.asNumeric()
            }

            this.profit.push(profit)
            this.totalProfit += profit.absDelta
        }
    }

    const genProfitSummary = portfolio => Object.keys(portfolio).
        reduce((acc, slug) => {
            const totalCoinProfit = acc[slug] = portfolio[slug].totalProfit
            acc.TOTAL += totalCoinProfit
            return acc
        }, { TIMESTAMP: Date.now(), TOTAL: 0 })

    const genRawCoinContainer = (acc, rawPortfolioData) => {
        // if acc === null acc = {} ===> return acc
        const container = acc[rawPortfolioData.coin] = acc[rawPortfolioData.coin] || { initialInvesment: [], profit: [], totalProfit: 0 }
        return {
            process: () => {
                Object.keys(portfolioDataComputers).
                    map(k => portfolioDataComputers[k].bind(container, rawPortfolioData)).
                    forEach(f => f())

                return acc
            }
        }
    }

    portfolio = await portfolioDataModel.getRows().
        then(rows => {
            logger.debug(`Got ${rows.length} buy transactions from cloud store`)
            return rows
        }).
        then(rows => rows.
            filter(row => row.coin !== 'TOTAL').
            reduce((acc, row) => genRawCoinContainer(acc, row).process(), {}))

    const profitSummary = await genProfitSummary(portfolio)

    return { portfolio, profitSummary, dataModel: portfolioDataModel }
}

/**
 * @returns {{rawCoins}, {profitHistogram}, {portfolio}, {rates}}
 */
CoinService.prototype.processCloudData = async function processCloudData() {
    const [profitHistogram, portfolio, coinsBindings] = await this.persistanceService().
        then(async db => {
            logger.debug("Fetching and processing cloud data...")

            const tasks = [
                Promise.resolve(db.profitHistogram),
                coinService.normalizePortfolio(db.portfolio),
                coinService.coinsBindingsFactory(db.coinsMeta, db.rates)
            ]

            return await Promise.all(tasks)
        })

    logger.debug("Cloud data fetched and normalized correctly") // tODO verify

    // All data ready. Build coins
    return coinFactory.bulk(portfolio, coinsBindings).
        then(async cryptoCoins => {
            return {
                cryptoCoins,
                profitHistogram,
                portfolio,
                rates: coinsBindings.resolvedRates
            }
        })
}

const coinService = new CoinService()
module.exports = coinService