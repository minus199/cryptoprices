const { Fiat, Crypto } = require("../data/Coins")
const CoinFactory = function CoinFactory() { }

CoinFactory.prototype.bulk = async function bulk(portfolio, { coins, ratesDataModelsBySlug, resolvedRates }) {
    coins.fiat = coins.fiat.map(slug => new Fiat(slug))

    // Replace raw fiat coins with FiatCoin
    const cryptoCoinsBySlug = Object.keys(coins.crypto).reduce((acc, cryptoSlug) => {
        acc[cryptoSlug] = coins.crypto[cryptoSlug].
            map(_bcs => coins.fiat.filter(c => c.slug === _bcs).pop() || _bcs)

        return acc
    }, {})

    /* cryptofolio */
    return Object.keys(coins.crypto).
        // Create CryptoCoin from raw crypto bindings    
        map(slug => {
            const args = [resolvedRates, ratesDataModelsBySlug, portfolio, cryptoCoinsBySlug].map(g => g[slug])
            return new Crypto(slug, ...args)
        }, {}).

        // Fix CryptoBinding to use CryptoCoin instead of raw coin slug
        map((currentCrypto, i, acc) => {
            currentCrypto.boundCoins = currentCrypto.boundCoins.
                map(boundCoin => acc.filter(bc => bc.slug === boundCoin)[0] || boundCoin)
            return currentCrypto
        })

    // TODO: add profit historgram to CryptoCoin
}

const coinFactory = module.exports = new CoinFactory()
