const logger = require("winston")
const { coinService } = require("../core/services")

const CryptoPortfolio = function () {
	this.cryptoCoins = null
	this.profitHistogram = null
	this.portfolio = null
	this.rates = null
	this.delta = null
	this.lastUpdate = 0
}

CryptoPortfolio.prototype.populate = async function populate() {
	if (this.coins)
		return this.coins

	logger.info("Starting cloud data processing...")
	Object.assign(this, await coinService.processCloudData())
	logger.info("Finished cloud data processing.")

	// Update timestamp of last crytpofolio full-round update
	this.lastUpdate = new Date()

	// Write sample to historgram
	this.profitHistogram.create(this.portfolio.profitSummary).then(profitHistogramEntry => {
		logger.debug("Wrote current sample to cloud log.")
		this.computeDelta()
	})

	return this
}

CryptoPortfolio.prototype.computeDelta = async function computeDelta() {
	logger.debug("Computing delta between previous sample and current.")

	const rows = await this.profitHistogram.getRows()
	const previousSample = rows[rows.length - 2]
	const currentSample = rows[rows.length - 1]

	this.delta = Object.keys(this.portfolio.profitSummary).map(slug => {
		const container = {
			slug, delta: 0,
			previous: previousSample[slug.toLowerCase()].asNumeric(),
			current: currentSample[slug.toLowerCase()].asNumeric()
		}

		container.delta = Math.ceil(container.current - container.previous)
		return container
	})

	// this.printProfitSummary()

	return this
}

CryptoPortfolio.prototype.printProfitSummary = function printProfitSummary() {
	const moment = require("moment"),
		prettyMS = require("pretty-ms"),
		columnify = require("columnify")

	return `\n${'-'.repeat(50)}\n${columnify(this.delta, {
		dataTransform: function (cellData, descriptor, index) {
			const keyName = descriptor.name.toLowerCase()
			if (index === 0 && cellData !== 'TIMESTAMP') {
				if (['previous', 'current'].includes(keyName)) {
					return `${moment.unix(cellData.asNumeric() / 1000).format('HH:mm:ss MMM DD')}`
				}

				if (keyName === 'delta') {
					return `${prettyMS(cellData.asNumeric())}`
				}
			}

			return keyName === 'slug' ? cellData : cellData.asCurrency()
		},
	})}\n${'-'.repeat(50)}\n`
}

module.exports = new CryptoPortfolio()