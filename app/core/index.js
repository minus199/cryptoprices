const logger = require("winston")
const cryptoPortfolio = require("./CryptoPortfolio")

module.exports = {
    populate: async () => cryptoPortfolio.populate(),
    printsummary: () => cryptoPortfolio.printProfitSummary() 
}
