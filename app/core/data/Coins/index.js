module.exports = {
    Coin: require("./Coin"),
    Crypto: require("./CryptoCoin"),
    Fiat: require("./FiatCoin")
}