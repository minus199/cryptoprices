const logger = require("winston")

const Coin = function (slug) {
    this.slug = slug
}

Coin.prototype.toString = function () {
    return `${this.coinType}-${this.slug}`
}

module.exports = Coin