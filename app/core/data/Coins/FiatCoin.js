const Coin = require("./Coin")

const FiatCoin = function(...args){
    Coin.call(this, ...args)
}
Object.assign(FiatCoin.prototype, Coin.prototype)

FiatCoin.prototype.TYPE = Symbol('fiat')

module.exports = FiatCoin