const logger = require("winston")
const Coin = require("./Coin")

const CryptoCoin = function (slug, rates, ratesDataModel, portfolio, boundCoins) {
    Coin.call(this, slug)
    this.boundCoins = boundCoins
    this.ratesDataModel = ratesDataModel
    this.rates = rates
    this.portfolio = portfolio
    this.crytpoRatesURL = null
    this.lastupdate = 0
}

Object.assign(CryptoCoin.prototype, Coin.prototype, {
    portfolio: null,
    crytpoRatesURL: null,
    ratesDataModel: null,
    rates: null,
    boundCoins: null,
    lastupdate: null
})

//set properties getters setters for isCorr and last update
CryptoCoin.prototype.persistRates = async function persistRates() {
    if (!this.ratesDataModel || Date.now() - this.lastupdate < 10000) {
        return this
    }

    logger.debug(`Rates were set for ${this.slug}, Updating rates data on cloud...`)

    for (rate in this.rates) {
        this.ratesDataModel[rate.toLowerCase()] = this.rates[rate]
    }

    this.ratesDataModel.save()
    this.lastupdate = Date.now()
    logger.debug(`Done updating rates sheet for ${this.slug}`)

    return this
}

module.exports = CryptoCoin