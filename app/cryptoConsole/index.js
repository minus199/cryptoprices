// Main console index
require("chalk")

const repl = require('repl')
const logger = require("winston")
const { cmdFactory } = require("./commands")

const CommandEvaluator = function (cmd, context, filename, callback) {
    const CommandExecutorResolver = (thisArg, ...inputCmds) => {
        const Cmd = cmdFactory.availableCommands.
            filter(cmd => cmd === inputCmds[0]).
            map(_hn => cmdFactory.cmdModules[_hn])[0]

        return {
            execute: thisArg => {
                const cbArgs = Cmd ? [null, new Cmd(...inputCmds).run()] : ['Command Not Found', null]
                return callback.call(thisArg, ...cbArgs)
            }
        }
    }

    const CommandsParser = cmd => cmd.split(" ").map(c => c.trim())

    CommandExecutorResolver(this, ...CommandsParser(cmd)).execute(this)
}

// start the console by invoking node app --console
if (process.argv.filter(arg => arg === '--console' || arg === '-C')) {
    logger.info("Starting crypto console")

    const replServerConfig = {
        prompt: '[CryptoConsole]> ',
        writer: output => [output, JSON.stringify(output)],
        eval: CommandEvaluator,
        completer: require("./completer")
    }

    const server = repl.start(replServerConfig).
        on('reset', function () {
            this.context = cryptofolio/* 
            Object.defineProperty(this.context, 'cryptofolio', {
                configurable: false,
                enumerable: true,
                value: { coins: 'will be here' }
            }) */
        }).
        on('exit', () => {
            this.close()
            console.log('Received "exit" event from repl!');
            process.exit();
        })
}