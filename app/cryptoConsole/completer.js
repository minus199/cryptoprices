const { cmdFactory } = require("./commands")
const availableCommands = cmdFactory.availableCommands

module.exports = function cryptoCommandsCompleter(partialCmd) {    
    if (!partialCmd) {
        return [availableCommands, partialCmd || '']
    }

    const matchByPartialCmd = availableCommands.filter(suggestion => suggestion.includes(partialCmd))
    if (matchByPartialCmd.length === 1) {
        return [matchByPartialCmd, partialCmd]
    }

    const matchByContainingChars = partialCmd.split('').reduce((suggestions, currentChar) => {
        return suggestions.filter(suggested => suggested.includes(currentChar))
    }, availableCommands)
    if (matchByContainingChars.length) {
        return [matchByContainingChars, partialCmd]
    }

    return [[], partialCmd]
}