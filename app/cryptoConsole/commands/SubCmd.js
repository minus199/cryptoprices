const BaseCmd = require("../BaseCmd")
const BaseSubCmd = function BaseSubCmd(...args) {
    BaseCmd.call(this, ...args)
}

module.exports = BaseSubCmd
