const BaseCmd = module.exports = function BaseCmd(slug, discoverySlug, helpText, ...subCmds) {
    this.slug = slug
    this.discoverySlug = discoverySlug
    this.helpText = helpText
    this.subCmds = subCmds
}

BaseCmd.prototype.matchRawUserInput = function (rawInputCmdString) {
    return `${rawInputCmdString}` === this.slug
}

BaseCmd.prototype.setToReplConsole = function setToReplConsole(replServer) {
    const runFunc = this.run.bind(this)
    replServer.defineCommand(this.slug, {
        help: this.helpText || `Help for ${this.slug} is missing. Please add`, // on .help
        action() { // `this` ctx will be repl server inside current code block
            return runFunc() // this will call .run() method of the cmd with the Cmd obj as `this` ctx
        }
    })
}

BaseCmd.prototype.run = function run() {
    console.info(`Tried to execute empty command for ${this.slug}`)
    return { func: this.slug, subCmds: this.subCmds }
}

module.exports = BaseCmd