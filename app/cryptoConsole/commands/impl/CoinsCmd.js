const BaseCmd = require("../BaseCmd")
const CoinsCmd = function CoinsCmd(...additionalCmdArgs) {
    BaseCmd.call(this, 'CoinsCmd', ...additionalCmdArgs)
}

CoinsCmd.prototype.run = function () {
    return this.subCmds.length > 0
        ? { coins: `selected coins -- ${this.subCmds.join(', ')}` }
        : { coins: 'this are all the coins' }
}

module.exports = CoinsCmd