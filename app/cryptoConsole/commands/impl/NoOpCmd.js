const BaseCmd = require("../BaseCmd")
const NoOpCmd = function NoOpCmd(...additionalCmdArgs){BaseCmd.call(this, 'NoOpCmd', ...additionalCmdArgs)}
NoOpCmd.discoverySlug = undefined
NoOpCmd.prototype.run = function(){
    console.log(`NoOpCmd triggered for ${'==missing=='}`)
}

module.exports = NoOpCmd