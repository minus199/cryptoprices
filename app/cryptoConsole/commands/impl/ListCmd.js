const BaseCmd = require("../BaseCmd")
const ListCmd = function ListCmd(...additionalCmdArgs) {
    BaseCmd.call(this, 'ListCmd', ...additionalCmdArgs)
}
const chalk = require("chalk")

ListCmd.prototype.run = function () {
    const coins = this.cryptofolio.cryptoCoins.
        map(cCoin => `${chalk.underline.bgGreen(cCoin.slug)} -- ${cCoin.boundCoins.map(c => chalk.bold.red(c.slug)).join(", ")}`)

    coins.forEach(c => console.info(c))
    return {}
}

module.exports = ListCmd