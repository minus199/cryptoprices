const logger = require("winston")
const BaseCmd = require("../BaseCmd")

const PortfolioCmd = function PortfolioCmd(...additionalCmdArgs) {
    BaseCmd.call(this, 'PortfolioCmd', ...additionalCmdArgs)
}

PortfolioCmd.prototype.run = function PortfolioCmd() {
    if (this.subCmds[0] === 'summary') {
        return { func: 'sub cmd summary', output: this.printProfitSummary() }
    }

    return { func: this.subCmds, output: "Nothing found" }
}

module.exports = PortfolioCmd