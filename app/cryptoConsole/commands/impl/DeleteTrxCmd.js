const BaseCmd = require("../BaseCmd")
const DeleteTrxCmd = function DeleteTrxCmd(...additionalCmdArgs) {
    BaseCmd.call(this, 'DeleteTrxCmd', ...additionalCmdArgs)
}

module.exports = DeleteTrxCmd