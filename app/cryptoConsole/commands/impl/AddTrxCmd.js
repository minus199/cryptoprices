const BaseCmd = require("../BaseCmd")
const AddTrxCmd = function AddTrxCmd(...additionalCmdArgs) {
    BaseCmd.call(this, 'AddTrxCmd', ...additionalCmdArgs)
}
module.exports = AddTrxCmd