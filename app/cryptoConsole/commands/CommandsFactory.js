// Commands index
const fs = require("fs")
const BaseCmd = require("./BaseCmd")

const excludedCommands = ['NoOpCmd', 'SubCmd', 'BaseCmd']
const commandsBaseFolder = require("path").dirname(module.filename) + '/impl'
const _initializedCache = { availableCmdModuleClasses: {}, availableCmdsByName: [], isEmpty: true }

const CommandsFactory = function (cryptofolio) {
    this.cryptofolio = cryptofolio
}

CommandsFactory.prototype = {
    locator() {
        return fs.readdirSync(commandsBaseFolder).
            map(cmdFile => cmdFile.split("").splice(0, cmdFile.lastIndexOf(".")).join("")) // trim file extension
            // filter(fn => fn !== 'index' && !excludedCommands.includes(fn)) // remove index.js and excluded cmds from the list of files
    },

    extender(cmdFile) {
        const Cmd = require(`${commandsBaseFolder}/${cmdFile}`) // require by filename in current dir

        Object.assign(
            Cmd.prototype,
            Object.create(BaseCmd.prototype),
            Object.create(Cmd.prototype),
            { cryptofolio: this.cryptofolio }
        )

        Cmd.prototype.constructor = Cmd
        Cmd.prototype.discoverySlug = Cmd.prototype.discoverySlug !== null
            ? Cmd.prototype.discoverySlug
            : cmdFile

        return Cmd
    },

    discoverer() {
        if (_initializedCache.isEmpty) {
            this.locator().reduce((_cache, cmdFile) => {
                const cmdModule = this.extender(cmdFile)
                _cache.availableCmdModuleClasses[cmdModule.name.replace(/Cmd{1,}$/, '')] = cmdModule
                return _cache
            }, _initializedCache)

            _initializedCache.availableCmdsByName = Object.freeze(Object.keys(_initializedCache.availableCmdModuleClasses))
            _initializedCache.isEmpty = false
        }

        return _initializedCache
    },

    get availableCommands() {
        return this.discoverer().availableCmdsByName
    },

    get cmdModules() {
        return this.discoverer().availableCmdModuleClasses
    }
}

module.exports = CommandsFactory
