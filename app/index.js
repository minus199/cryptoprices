#!/usr/bin/node

const logger = require('winston')
logger.add(logger.transports.File, { filename: 'mainLog.log' })
logger.level = 'debug'
// logger.remove(logger.transports.Console)

const numeral = require("numeral")
Object.defineProperties(String.prototype, {
    asNumeric: {
        value: function asNumeric() {
            return this.asNumeral().value()
        }
    },
    asNumeral: {
        value: function asNumeric() {
            return numeral(`${this}`)
        }
    },
    asCurrency: {
        value: function formatNumeral() {
            return this.asNumeral().format('$0,0.0')
        }
    }
})

Object.defineProperties(Array.prototype, {
    pump: {
        value: function pump() {
            Array.prototype.push.call(this, ...arguments)
            return this
        }
    }
})

const cryptoFolio = require("./core")
module.exports = async () => {
    logger.debug("Starting check")
    global.cryptofolio = await cryptoFolio.populate()
    
    require("./cryptoConsole")
}


Promise.resolve(module.exports())