# README #

## Config ##
```
app/resources
├── client_secret.json  -- Get from google dev console. Create a new service account, copy the creds json file as this file
└── gspreadConfig.json -- get the sheet id from the browser when going to the spreadsheet, copy into this file
```
After duplicating the worksheet files, share it with `client_email` from the client_secret.json file

## Sheets ##
Notice which cells are has absolute value and which has a formula. 
Add buy trxs buy right click -> add new row -> dragging cells which has formula.
 * Portfolio - a list of buy transaction for portfolio. Add a row after each time crypto is added to the portfolio. Make sure to drag cells where ever possible.
 * Rates - auto
 * ProfitHistogram - auto
 * CoinMeta - Specify which coins are used

Install and run:
 * `git clone https://minus199@bitbucket.org/minus199/cryptoprices.git && cd $_`
 * `npm install`
 * `node app`
Log will be written to mainLog in root folder of app